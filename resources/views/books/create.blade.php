@extends('layouts.app')
@section('content')


<h1>Add a Book</h1>

<form method = 'post' action = "{{action('BookController@store')}}" >

@csrf
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class = "form-group">
<label for = "title" >Add a title:</label>
<input type = "text" class = "form-control" name = "title" >
</div>


<div class = "form-group">
<label for = "author" >Add an Author:</label>
<input type = "text" class = "form-control" name = "author" >
</div>




<div class = "form-group">
<input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>





</form>
@endsection