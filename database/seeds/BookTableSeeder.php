<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([[
            'title' => 'Buy milk',
            'author' => 'Avi Malichi',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 1,
        ],
        [
            'title' => 'prepare for the test',
            'author' => 'David Dor',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 2,
        ],
        [
            'title' => 'Read a book',
            'author' => 'Yarden Muallem',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 1,
        ],
        [
            'title' => 'Hello World',
            'author' => 'Roni Horowitz',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 3,
        ],
        [
            'title' => 'Reading a Book',
            'author' => 'Yarden Muallem',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 1,
        ],
        [
            'title' => 'Hello Hello',
            'author' => 'Roni Horowitz',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 5,
        ]
        ]
        );  
        
    }
}