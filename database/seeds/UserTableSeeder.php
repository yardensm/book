<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    
                    'name' => 'Yarden Muallem',
                    'email' => 'yarden@gmail.com',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    
                    'name' => 'Ayelet Siani',
                    'email' => 'ayelet@gmail.com',
                    'password' => '1235',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    
                    'name' => 'Sahar Sabtai',
                    'email' => 'Sahar@gmail.com',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
}
